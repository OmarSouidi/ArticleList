'use strict';

import React, {Component} from 'react';
import { Container, Content, List, ListItem, Thumbnail, Text, Body } from 'native-base';




export default class Home extends Component {
  
   render() {
    return (
      <Container>
        <Content>
          <List>
            <ListItem>
              <Thumbnail square size={80} source={{ uri: 'https://finance.rabbit.co.th/images/nana/cms/80x80x4fd46ecfa69e8868159ea9de29d7286c.jpg.pagespeed.ic.1exk-BUwP4.jpg' }} />
              <Body>
                <Text>Article 1</Text>
                <Text note>Ceci est le 1ér article</Text>
              </Body>
            </ListItem>
             <ListItem>
              <Thumbnail square size={80} source={{ uri: 'https://finance.rabbit.co.th/images/nana/cms/80x80x4fd46ecfa69e8868159ea9de29d7286c.jpg.pagespeed.ic.1exk-BUwP4.jpg' }} />
              <Body>
                <Text>Article 1</Text>
                <Text note>Ceci est le 1ér article</Text>
              </Body>
            </ListItem>
             <ListItem>
              <Thumbnail square size={80} source={{ uri: 'https://finance.rabbit.co.th/images/nana/cms/80x80x4fd46ecfa69e8868159ea9de29d7286c.jpg.pagespeed.ic.1exk-BUwP4.jpg' }} />
              <Body>
                <Text>Article 1</Text>
                <Text note>Ceci est le 1ér article</Text>
              </Body>
            </ListItem>
             <ListItem>
              <Thumbnail square size={80} source={{ uri: 'https://finance.rabbit.co.th/images/nana/cms/80x80x4fd46ecfa69e8868159ea9de29d7286c.jpg.pagespeed.ic.1exk-BUwP4.jpg' }} />
              <Body>
                <Text>Article 1</Text>
                <Text note>Ceci est le 1ér article</Text>
              </Body>
            </ListItem>
          </List>
        </Content>
      </Container>
    );
  }
}
module.exports = Home;